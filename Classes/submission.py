from typing import List, Optional, Dict
from pydantic import BaseModel
import datetime

class Submission(BaseModel):
    your_email_address: str
    unique_emails: List[str]
    user_domain_counts: Dict[str, int]
    april_emails: List[str]

