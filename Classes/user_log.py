from typing import List, Optional
from pydantic import BaseModel
import datetime
import pytz

class UserLog(BaseModel):
    email: Optional[str]
    login_date: Optional[str]
    def login_date_utc(self):
        return datetime.datetime.fromisoformat(self.login_date).astimezone(pytz.utc)