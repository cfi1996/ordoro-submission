## Running the code

### Poetry
The easiesy to run the code is to use [Poetry](https://python-poetry.org/)
Once you have poetry installed, from a terminal, run

`poetry install` 

to install the project dependencies.
After which, you can use  `poetry run python3 main.py` to run the code.


### Without Poetry
To run the code without poetry, make sure that the dependencies are installed.

A pip style requirements.txt is included. 

The dependencies can be installed by running `pip install -r requirements.txt` from a terminal.
After which `python3 main.py` can be used to run the code.
