import re # needed for regex extraction
import requests
import json 
from Classes.user_log import UserLog
from Classes.submission import Submission
from pydantic import BaseModel, parse_obj_as
from typing import List, Dict

def main():
    user_logs = load_logs()
    emails = [log.email for log in user_logs if log.email is not None]
    uniques = get_unique(emails)
    domain_counts = {key:value for (key,value) in (count_domains(emails).items()) if value > 1}
    april_logins = [log.email for log in user_logs if log.login_date is not None and len(log.login_date) > 0 and log.login_date_utc().month == 4]
    submission = Submission(your_email_address="chris.iossa@gmail.com",
        unique_emails=uniques,
        user_domain_counts=domain_counts,april_emails=april_logins)
    print(submission.json())
    print()
    resp = submit(submission)
    print(resp)
    print(resp.json())
    

def load_logs() -> List[UserLog]:
    endpoint = 'https://us-central1-marcy-playground.cloudfunctions.net/ordoroCodingTest'
    log_dict = requests.get(endpoint).json()["data"]
    user_logs = parse_obj_as(List[UserLog],log_dict)
    return user_logs

def submit(submission: Submission) -> str:
    endpoint = 'https://us-central1-marcy-playground.cloudfunctions.net/ordoroCodingTest'
    resp = requests.post(endpoint, json = submission.dict())
    return resp

def get_unique(lst: str) -> List[str]:
    return list(set(lst)) 


def count_domains(lst: List[str],) -> Dict:
    dict = {} 
    for domain, addr in zip (map(extract_domain, lst), lst): # zip input list into list of pairs, domain and lst
        if domain not in dict: 
                dict[domain] = 1 # note that is indexed from 1 for informative purposes
        else:
            dict[domain] += 1
    return dict

def extract_domain(addr: List[str]) -> str:
    # RFC 5322 regex from emailregex.com, modified to add capture group
    emailRegex = r"(^[a-zA-Z0-9_.+-]+@([a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)$)"
    matchObj = re.search(emailRegex, addr.strip()) # check if email conforms to RFC 5322 structure. trimming because whitespace is not valid
    if matchObj is not None:  # if RFC 5322 email
        return matchObj.group(2)# return domain portion (from Regex capture group). 
    else: # if not RFC 5322, we still want to handle the data
        # recover by performing naieve split on @ char and returning the rightmost substring, trimming because whitespace is not valid
        return addr.strip().split("@")[-1] 

if __name__ == "__main__":
    main()
